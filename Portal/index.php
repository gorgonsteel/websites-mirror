  

    <!DOCTPYPE html>
    <html>
      <!--
         * Aksel Telle's portal page.
         *
      -->
      <head>
        <title>Aksel Telle</title>
        <link rel="icon" type="image/png" href="img/bg.jpg">
        <style>
        html,body{
  margin:0;
  padding:0;
  height:100%;
  width:100%;
  overflow:hidden;
  font-family:Helvetica,Arial,Sans-Serif;
}


body{
  background-image:url('img/bg.jpg');
  background-size:cover
}

.container{
  height:100%;
  width:100%;
  max-width:100%;
  overflow:auto
}

.top{
  height:100%;
  width:100%;
  position:relative
}

.top .main{
  width:100%;
  text-align:center;
  font-size:24px;
  padding-top:25px
}

.top h1{
  font-weight:bold;
  margin:0
}

.orb{
  position:absolute;
  margin:0 auto;
  left:0;
  right:0;
  top:calc(100% - 40px);
  height:80px;
  width:80px;
  border-radius:50%;
  background-color:#eee
}

.bottom{
  height:444px;
  width:100%;
  background:#eee
}

.button{
  height:65px;
  width:200px;
  margin:5px;
  display:inline-block;
  background:black;
  border-bottom:2px solid black;
  border-radius:3px;
  line-height:65px;
  text-align:center;
  font-size:22px;
  color:gold;
  user-select:none;
  -webkit-user-select:none;
  -ms-user-select:none
}

.button:hover{
  color:gold;
  background:black;
  border-color:gold;
  cursor:pointer
}

.button:active{
  color:black;
  background:gold;
  border-color:black
}

.pcounter {
    background: none repeat scroll 0 0 #eee;
    border-radius: 2px;
    box-shadow: 0 0 7px #444;
    color: #76c479;
    height: 45px;
    line-height: 25px;
    margin: 160px auto 0 140px;
    padding: 10px 0;
    position: absolute;
    text-align: center;
    width: 350px;
}

#popup_container{
  position:absolute;
  z-index:99999;
  top:0;
  background:rgba gold;
  transition:opacity .5s ease;
  visibility:hidden;
  opacity:0
}

#popup{
  position:absolute;
  margin:auto;
  top:0;
  right:0;
  bottom:0;
  left:0;
  width:92.5%;
  height:87.5%;
  background:#0B0B0B;
  border-radius:5px;
  box-shadow:0px 0px 15px #181818
}

#popup_close {
    background-position: -16px 0;
    cursor: pointer;
    margin: -53px -8px 0 0;
    position: absolute;
    right: 0;
    top: 0;
}

#popup_right{
  position:absolute;
  top:50%;
  right:0;
  margin:-8px 32px 0
}

#popup_left{
  position:absolute;
  top:50%;
  left:0;
  margin:-8px 0 0 32px;
  background-position:-32px 0
}

#vote_indicator {
    border-radius: 5px;
    color: white;
    height: 24px;
    left: 10px;
    line-height: 24px;
    margin: -40px 0 0;
    padding: 0 10px;
    position: absolute;
    top: 0;
}

#vote_alert {
    border-radius: 5px;
    color: #eee;
    font-weight: bold;
    height: 24px;
    left: 0;
    line-height: 24px;
    margin: -40px auto 0;
    padding: 0 10px;
    position: absolute;
    right: 0;
    text-align: center;
    text-shadow: 0 0 4px #999;
    top: 0;
    width: 500px;
}

.popup_clickbar {
    cursor: pointer;
    height: 50px;
    margin: 25% auto;
    position: absolute;
    width: 100px;
    z-index: 9999;
    border-radius: 15px;
}

.popup_button_l{
	background-image: url("http://www.xtridence.de/vanillaverse/left.png");
	background-repeat: no-repeat;
    height: 45px;
    width: 95px;
}

.popup_button_l:hover{
  opacity: 0.5;
}

.popup_button_l:active{
  background-color:rgba(100,100,100,0.2)
}

.popup_button_r{
	background-image:url("http://www.xtridence.de/vanillaverse/right.png");
	background-repeat: no-repeat;
	background-position: left;
    height: 45px;
    width: 95px;
}

.popup_button_r:hover{
  opacity: 0.5;
}

.popup_button_r:active{
  background-color:rgba(100,100,100,0.2)
}

.popup_button_c{
	background-image:url("http://www.xtridence.de/vanillaverse/close.png");
	background-repeat: no-repeat;
    height: 45px;
    width: 95px;
}

.popup_button_c:hover{
  opacity: 0.5;
}

.popup_button_c:active{
  background-color:rgba(100,100,100,0.2)
}

.support {
    background: none repeat scroll 0 0 #eee;
    border-radius: 2px;
    color: #76c479;
    height: 45px;
    line-height: 25px;
    margin: 222px auto 0 140px;
    padding: 10px 0;
    position: absolute;
    text-align: center;
    width: 350px;
}

        </style>
      </head>
      <body>
	  <center>
        <div class=container>
          <div class=top>
            <div class=main><h1><img src="img/logo.png"/></h1></div><br><br>
              <div class=button onclick="location.href='#'">Forum</div>
              <div class=button onclick="location.href='#'">Store</div>
              <div class=button onclick=votePopup()>Vote</div>
               
                  </center>
          
            </div>
            
        <script>
          function popup(a){
            if(!document.getElementById('popup_container')){
              var pop_con           = document.createElement('div');
              pop_con.id            = 'popup_container';
              pop_con.style.width   = document.documentElement.clientWidth+'px';
              pop_con.style.height  = document.documentElement.clientHeight+'px';
              pop_con.innerHTML     = '<div id=popup>'+a+'</div>';
              pop_con.onclick       = function(e){if(e.target==this){
                var thiso           = this;
                this.style.opacity  = '0';
                document.body.style.overflow='auto';
                setTimeout(function(){document.body.removeChild(thiso)},500);
              }};
              document.body.appendChild(pop_con);
              setTimeout(function(){
                document.body.style.overflow='hidden';
                window.scrollTo(0,0);
                document.getElementById('popup_container').style.visibility='visible';
                document.getElementById('popup_container').style.opacity='1';
              },10);
            }
          }
     
         
          var votealert = "You want to keep voting? Use Previous and Continue to browse!";
     
          var votelinks =   ['http://minecraft-mp.com/',
                             'http://minecraftservers.org/',
                             'http://www.planetminecraft.com/'];
     
          var curvcount=0;
          function scrollPage(a){
            if(curvcount+a>=votelinks.length){
              curvcount=0;
            }else if(curvcount+a<0){
              curvcount=votelinks.length-1;
            }else{
              curvcount+=a;
            }
            document.getElementById('vote_indicator').innerHTML=votelinks[curvcount].split('/')[2]+' ('+(curvcount+1)+'/'+votelinks.length+')';
            document.getElementById('vf').src=votelinks[curvcount];
          }
          function votePopup(){popup("<div class=popup_button_c id=popup_close onclick=document.getElementById('popup_container').click()></div> \
                                      <div class=popup_clickbar onclick=scrollPage(-1) style=left:25><div class=popup_button_l></div></div> \
                                      <div class=popup_clickbar onclick=scrollPage(1) style=right:25><div class=popup_button_r></div></div> \
                                      <div id=vote_indicator class=c1>"+votelinks[curvcount].split('/')[2]+' ('+(curvcount+1)+'/'+votelinks.length+')'+"</div> \
                                      "+(function(){if(votealert!=''&&votealert!==null){return '<div id=vote_alert>'+votealert+'</div>'}else{return ''}})()+" \
                                      <iframe width=100% height=100% id=vf src="+votelinks[curvcount]+" style=border:0;border-radius:5px>Your browser does not support iFrames!</iframe>")}
        </script>
      </body>
    </html>

